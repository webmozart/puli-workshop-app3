<?php

namespace Acme\Application;

use Closure;
use Puli\TwigExtension\PuliExtension;
use Puli\TwigExtension\PuliTemplateLoader;
use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Twig_Environment;

class DemoApplication extends Application
{
    public function __construct()
    {
        parent::__construct();

        $this->register(new TwigServiceProvider());

        /** @var Closure $twig */
        $twig = $this->raw('twig');

        $this['twig.options'] = array(
            'cache' => realpath(__DIR__.'/..').'/var/cache/twig',
        );

        $this['twig'] = $this->share(function (Application $app) use ($twig) {
            $twig = $twig($app);

            /** @var Twig_Environment $twig */

            // TODO add Puli extension

            return $twig;
        });

        $this->mount('', new ControllerProvider());
    }
}
